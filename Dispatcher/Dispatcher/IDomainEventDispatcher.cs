using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace Dispatcher.Dispatcher
{
    interface IDomainEventDispatcher
    {
        Task DispatchAsync<TDomainEvent>(TDomainEvent @event) where TDomainEvent : IDomainEvent;
    }

    interface IDomainEvent  
    {
        DateTime EventDate { get; }
    }

    abstract record DomainEvent : IDomainEvent
    {
        public DateTime EventDate { get; } = DateTime.Now;
    }

    interface IDomainEventHandler<in TDomainEvent> where TDomainEvent : IDomainEvent
    {
        Task HandleAsync(TDomainEvent @event);
    }
    
    class ServiceCollectionDomainEventDispatcher : IDomainEventDispatcher
    {
        private static readonly Type DomainEventHandlerGenericType = typeof(IDomainEventHandler<>);
        private readonly ILogger<ServiceCollectionDomainEventDispatcher> _logger;
        private readonly IServiceProvider _serviceProvider;

        public ServiceCollectionDomainEventDispatcher(
            ILogger<ServiceCollectionDomainEventDispatcher> logger,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public Task DispatchAsync<TDomainEvent>(TDomainEvent @event) where TDomainEvent : IDomainEvent
        {
            var handlerType = DomainEventHandlerGenericType.MakeGenericType(@event.GetType());
            
            var handlers = _serviceProvider
                    .GetServices(handlerType)
                    .OfType<IDomainEventHandler<TDomainEvent>>();

            return Task.WhenAll( handlers.Select(handler =>
            {
                try
                {
                    return handler.HandleAsync(@event);
                    // var method = handlerType.GetMethod(nameof(IDomainEventHandler<IDomainEvent>.HandleAsync));
                    //
                    // return (Task)method.Invoke(handler, new object?[] {@event});
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Event handler {EventHandler} failed to handle event {Event}", handler.GetType(), @event);
                    return Task.FromException(e);
                }
            }));
        }
    }
    
    static class DomainEventHandlingServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainEventHandling(this IServiceCollection services)
        {
            services.TryAddScoped<IDomainEventDispatcher, ServiceCollectionDomainEventDispatcher>();

            var handlerType = typeof(IDomainEventHandler<>);

            return typeof(IDomainEvent).Assembly
                .GetTypes()
                .Where(x => x.IsClass)
                .Where(x => !x.IsAbstract)
                .Where(x => x.ImplementsGenericInterface(handlerType))
                .SelectMany(x =>
                    {
                        if (x.IsGenericType)
                        {
                            return new[] { handlerType };
                        }
                        return x.GetInterfaces()
                            .Where(y => y.IsGenericType && y.GetGenericTypeDefinition() == handlerType);
                    },
                    (impl, @interface) => new { impl, @interface } )
                .Aggregate(services, (s, handler) =>
                {
                    if (handler.impl.IsGenericType)
                    {
                        s.AddScoped(handlerType, handler.impl.GetGenericTypeDefinition());
                    }
                    else
                    {
                        s.AddScoped(handler.@interface, handler.impl);
                    }
                    return s;
                });
        }
    }
}