using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Dispatcher.Dispatcher
{
    public interface ICommandDispatcher
    {
        Task<TResult> DispatchAsync<TResult>(ICommand<TResult> command);
    }

    public interface ICommand<TResult>
    {
    }
    
    interface ICommandHandler<in TCommand, TResult> where TCommand : ICommand<TResult>
    {
        Task<TResult> HandleAsync(TCommand command);
    }

    class ServiceCollectionCommandDispatcher : ICommandDispatcher
    {
        private static readonly Type CommandHandlerGenericType = typeof(ICommandHandler<,>);
        private readonly IServiceProvider _serviceProvider;

        public ServiceCollectionCommandDispatcher(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

        public Task<TResult> DispatchAsync<TResult>(ICommand<TResult> command)
        {
            var handlerType = CommandHandlerGenericType.MakeGenericType(command.GetType(), typeof(TResult));
            
            var handler = _serviceProvider.GetRequiredService(handlerType);

            var method = handlerType.GetMethod(nameof(ICommandHandler<ICommand<TResult>, TResult>.HandleAsync));

            return (Task<TResult>)method.Invoke(handler, new object?[] {command});
        }
    }

    static class CommandHandlingServiceCollectionExtensions
    {
        public static IServiceCollection AddCommandHandling(this IServiceCollection services)
        {
            services.TryAddScoped<ICommandDispatcher, ServiceCollectionCommandDispatcher>();

            var handlerType = typeof(ICommandHandler<,>);

            return typeof(IDomainEvent).Assembly
                .GetTypes()
                .Where(x => x.IsClass)
                .Where(x => !x.IsAbstract)
                .Where(x => x.ImplementsGenericInterface(handlerType))
                .SelectMany(x =>x.GetInterfaces().Where(y => y.IsGenericType && y.GetGenericTypeDefinition() == handlerType),
                    (impl, @interface) => new { impl, @interface } )
                .Aggregate(services, (s, handler) => s.AddScoped(handler.@interface, handler.impl));
        }
    }

    static class TypeExtensions
    {
        public static bool ImplementsGenericInterface(this Type typeToInspect, Type genericInterface) =>
            typeToInspect
                .GetInterfaces()
                .Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == genericInterface);
    }
}