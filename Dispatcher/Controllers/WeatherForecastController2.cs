﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dispatcher.Dispatcher;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dispatcher.Controllers
{
    [ApiController]
    [Route("/v2/WeatherForecast")]
    public class WeatherForecastController2 : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public WeatherForecastController2(ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
        }

        [HttpPost]
        public async Task<IActionResult> AddForecast([FromBody] WeatherForecast forecast)
        {
            var result = await _commandDispatcher.DispatchAsync(new AddForecastCommand(forecast));

            return result switch
            {
                AddResult.Created => StatusCode(201),
                AddResult.Updated => NoContent()
            };
        }

        [HttpGet("{dateString}")]
        public async Task<IActionResult> Get([FromRoute]string dateString)
        {
            var date = DateTime.Parse(dateString);

            var result = await _commandDispatcher.DispatchAsync(new GetForecastCommand(date));

            return result == null ? (IActionResult) NotFound() : Ok(result);
        }
    }

    record GetForecastCommand(DateTime Date) : ICommand<WeatherForecast>;
    
    class GetForecastCommandHandler : ICommandHandler<GetForecastCommand, WeatherForecast>
    {
        private readonly ILogger<GetForecastCommandHandler> _logger;
        private readonly IDictionary<DateTime, WeatherForecast> _weatherForecasts;

        public GetForecastCommandHandler(ILogger<GetForecastCommandHandler> logger,  IDictionary<DateTime, WeatherForecast> weatherForecasts)
        {
            _logger = logger;
            _weatherForecasts = weatherForecasts;
        }
        public Task<WeatherForecast> HandleAsync(GetForecastCommand command)
        {
            if (_weatherForecasts.ContainsKey(command.Date))
            {
                _logger.LogInformation("Increment get metric for {Date}", command.Date);
                return Task.FromResult(_weatherForecasts[command.Date]);
            }

            _logger.LogInformation("Request missing weather information {Date}", command.Date);
            
            return Task.FromResult((WeatherForecast)null);
        }
    }

    record AddForecastCommand(WeatherForecast Forecast) : ICommand<AddResult>;
    
    class AddForecastCommandHandler : ICommandHandler<AddForecastCommand, AddResult>
    {
        private readonly IDictionary<DateTime, WeatherForecast> _weatherForecasts;
        private readonly IDomainEventDispatcher _domainEventDispatcher;

        public AddForecastCommandHandler(IDictionary<DateTime, WeatherForecast> weatherForecasts, IDomainEventDispatcher domainEventDispatcher)
        {
            _weatherForecasts = weatherForecasts;
            _domainEventDispatcher = domainEventDispatcher;
        }
        
        public Task<AddResult> HandleAsync(AddForecastCommand command)
        {
            var forecast = command.Forecast;
            if (_weatherForecasts.ContainsKey(command.Forecast.Date))
            {
                _weatherForecasts[command.Forecast.Date] = forecast;

                _domainEventDispatcher.DispatchAsync(new WeatherForecastUpdatedEvent(forecast));

                return Task.FromResult(AddResult.Updated);
            }

            _weatherForecasts.Add(forecast.Date, forecast);
            
            _domainEventDispatcher.DispatchAsync(new WeatherForecastCreatedEvent(forecast));

            return Task.FromResult(AddResult.Created);
        }
    }

    enum AddResult
    {
        Created,
        Updated
    }

    record WeatherForecastCreatedEvent(WeatherForecast Forecast) : DomainEvent;
    record WeatherForecastUpdatedEvent(WeatherForecast Forecast) : DomainEvent;
    
    class MetricDomainEventHandler :
        IDomainEventHandler<WeatherForecastCreatedEvent>,
        IDomainEventHandler<WeatherForecastUpdatedEvent>
    {
        private readonly ILogger<MetricDomainEventHandler> _logger;

        public MetricDomainEventHandler(ILogger<MetricDomainEventHandler> logger) => _logger = logger;
        
        public Task HandleAsync(WeatherForecastCreatedEvent @event)
        {
            _logger.LogInformation("Increment metric for add {Weather}", @event.Forecast);
            return Task.CompletedTask;
        }
        
        public Task HandleAsync(WeatherForecastUpdatedEvent @event)
        {
            _logger.LogInformation("Increment metric for update {Weather}", @event.Forecast);
            return Task.CompletedTask;
        }
    }
    
    class AdminNotificationDomainEventHandler :
        IDomainEventHandler<WeatherForecastCreatedEvent>,
        IDomainEventHandler<WeatherForecastUpdatedEvent>
    {
        private readonly ILogger<AdminNotificationDomainEventHandler> _logger;

        public AdminNotificationDomainEventHandler(ILogger<AdminNotificationDomainEventHandler> logger) => _logger = logger;

        public Task HandleAsync(WeatherForecastCreatedEvent @event)
        {
            _logger.LogInformation("Send email to admin about create {Weather}", @event.Forecast);
            return Task.CompletedTask;
        }

        public Task HandleAsync(WeatherForecastUpdatedEvent @event)
        {
            _logger.LogInformation("Send email to admin about updated {Weather}", @event.Forecast);
            return Task.CompletedTask;
        }
    }
    
    class ReportingUpdateDomainEventHandler :
        IDomainEventHandler<WeatherForecastCreatedEvent>
    {
        private readonly ILogger _logger;

        public ReportingUpdateDomainEventHandler(ILogger<ReportingUpdateDomainEventHandler> logger) => _logger = logger;

        public Task HandleAsync(WeatherForecastCreatedEvent @event)
        {
            _logger.LogInformation("Update reporting system with new {Date}", @event.Forecast.Date);
            return Task.CompletedTask;
        }
    }
    
    class AuditHandler<T> : IDomainEventHandler<T> where T : IDomainEvent
    {
        private readonly ILogger<AuditHandler<T>> _logger;

        public AuditHandler(ILogger<AuditHandler<T>> logger) => _logger = logger;

        public Task HandleAsync(T @event)
        {
            _logger.LogInformation("Audit {DomainEvent}", @event);
            return Task.CompletedTask;
        }
    }
}