﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dispatcher.Controllers
{
    [ApiController]
    [Route("/v1/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IDictionary<DateTime, WeatherForecast> _weatherForecasts;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IDictionary<DateTime, WeatherForecast> weatherForecasts)
        {
            _logger = logger;
            _weatherForecasts = weatherForecasts;
        }

        [HttpPost]
        public IActionResult AddForecast([FromBody] WeatherForecast forecast)
        {
            if (_weatherForecasts.ContainsKey(forecast.Date))
            {
                _weatherForecasts[forecast.Date] = forecast;
                
                _logger.LogInformation("Increment metric for update {weather}", forecast);
                
                _logger.LogInformation("Send email to admin about updated {Weather}", forecast);
                
                return NoContent();
            }

            _weatherForecasts.Add(forecast.Date, forecast);
            
            _logger.LogInformation("Increment metric for add {Weather}", forecast);
            
            _logger.LogInformation("Update reporting system with new {Date}", forecast.Date);
            
            _logger.LogInformation("Send email to admin about create {Weather}", forecast);

            
            return StatusCode(201);
        }
        
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogInformation("Increment get metric of get all {Count}", _weatherForecasts.Count);
            return _weatherForecasts.Values;
        }

        [HttpGet("{dateString}")]
        public IActionResult Get([FromRoute]string dateString)
        {
            var date = DateTime.Parse(dateString);
            
            if (_weatherForecasts.ContainsKey(date))
            {
                _logger.LogInformation("Increment get metric for {Date}", date);
                return Ok(_weatherForecasts[date]);
            }

            _logger.LogInformation("Request missing weather information for {Date}", date);
            
            return NotFound();
        }
    }
}